﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMyScriptableObject : MonoBehaviour
{
    [SerializeField]
    MyScriptableObject _myScriptableObject;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.DownArrow)){
            _myScriptableObject._Hp--;
            Debug.Log(_myScriptableObject._Hp);
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow)){
            _myScriptableObject._Hp++;
            Debug.Log(_myScriptableObject._Hp);
        
        }

    }
}
