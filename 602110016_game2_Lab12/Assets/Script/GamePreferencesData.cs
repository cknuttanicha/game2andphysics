﻿using System;

[Serializable]
public class GamePreferencesData
{
    public string _playerName;
    public int _score;
    public float _musicVolume;
    public float _SFXVolume;
    public int _age;
}
