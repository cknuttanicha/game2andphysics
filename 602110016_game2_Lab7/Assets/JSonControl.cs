﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSonControl: MonoBehaviour
{
    Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _animator = this.GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            _animator.SetTrigger("Esquiva");
        }
         if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            _animator.SetTrigger("Dancing Maraschino Step");
        }
         if(Input.GetKeyDown(KeyCode.LeftAlt))
        {
            _animator.SetBool("Hit To Side Of Body",true);
        }
         else if(Input.GetKeyUp(KeyCode.LeftAlt))
        {
            _animator.SetBool("Hit To Side Of Body",false);
        }
         if(Input.GetKeyDown(KeyCode.A))
        {
            _animator.SetBool("Swing Dancing",true);
        }
         else if(Input.GetKeyUp(KeyCode.A))
        {
            _animator.SetBool("Swing Dancing",false);
        }
        
        
    }
}
