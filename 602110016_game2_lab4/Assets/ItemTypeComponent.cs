﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ItemTypeComponent : MonoBehaviour {
    [SerializeField]
    protected ItemType _itemType;
    public ItemType Type {
        get {
            return _itemType;
        }
        set {
            _itemType = value;

        }
    }
   
    }
