﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerCollisionCheck : MonoBehaviour {

    TextMeshPro _textMeshPro = null;

    // Use this for initialization
    void Start () {
        _textMeshPro = this.GetComponentInChildren<TextMeshPro> ();

    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Item") {
            Destroy (collision.gameObject);
        } else if (collision.gameObject.tag == "Obstacle") {

            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody> ();
            rb.AddForce (0, 10, 0, ForceMode.Impulse);

            Destroy (collision.gameObject, 2f);
        }

        ItemTypeComponent itc = collision.gameObject.GetComponent<ItemTypeComponent> ();

        if (itc != null) {
            switch (itc.Type) {
                case ItemType.BIGCOIN:
                    _textMeshPro.text = "BIGCOIN";
                    break;

                case ItemType.BOX_ITEM:
                    _textMeshPro.text = "Box Item";
                    break;

                case ItemType.COIN:
                    _textMeshPro.text = "COIN";
                    break;
                case ItemType.SPHERE_OBSTACLE:
                    _textMeshPro.text = "SPHERE_OBSTACLE";
                    break;
                case ItemType.Cylinder_Obstacle:
                    _textMeshPro.text = "Cylinder_Obstacle";
                    break;
                case ItemType.Capsule_Obstacle:
                    _textMeshPro.text = "Capsule_Obstacle";
                    break;
                case ItemType.POWERUP:
                    _textMeshPro.text = "POWERUP";
                    break;
                case ItemType.POWERDOWN:
                    _textMeshPro.text = "POWERDOWN";
                    break;
            }
        }

    }
}