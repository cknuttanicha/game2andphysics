﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControl2 : MonoBehaviour
{
 Vector3 _cubeMovementStep = new Vector3(0.05f,0.05f,0.05f);

 // Use this for initialization
 void Start () {

 }

 // Update is called once per frame
 void Update () {
 this.transform.position += _cubeMovementStep;

  if (this.transform.position.x >= 4.0f || this.transform.position.x <= -4.0f) {
 _cubeMovementStep.x *= -1.1f;
 }
 if (this.transform.position.y >= 4.0f || this.transform.position.y <= -4.0f)
 {
 _cubeMovementStep.y *=-1.1f;
 }
if (this.transform.position.z >= 3.0f || this.transform.position.z <= -3.0f)
 {
 _cubeMovementStep.z *= -1.1f;
 }
if (Input.GetMouseButtonDown(0)) {
 GameObject cube = GameObject.Find("Cube");
 Vector3 cubePosition = cube.transform.position;
 Vector3 vecToCubeNorm = cubePosition - this.transform.position;
 vecToCubeNorm.Normalize();

 this._cubeMovementStep = vecToCubeNorm/10.0f;
 }
 
 }
 }