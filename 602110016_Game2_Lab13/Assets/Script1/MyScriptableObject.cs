﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MyScriptableObject : ScriptableObject
{
    public string _objectName = "my object";
    public float _speedMagnitude = 1.0f;
    public float _fireRate = 1.0f; //The rate at which the gun is fired.
}