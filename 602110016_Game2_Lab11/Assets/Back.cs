﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Back : MonoBehaviour {
    [SerializeField] Button _backButton;
    void Start () {
        _backButton.onClick.AddListener (delegate { BackToMainMenuAll (_backButton); });
    }

    public void BackToMainMenuAll (Button button) {
         GameApplicationManager.Instance.IsOptionMenuActive = false;

        SceneManager.UnloadSceneAsync (SceneManager.GetActiveScene ());
        SceneManager.LoadScene ("SceneMainMenu");
    }

}
